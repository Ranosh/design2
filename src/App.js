import React, { Component } from 'react';
import {css , cx} from 'emotion'

const R = css`
#sidebar{
  position: absolute;
  width:250px;
  height:300px;
  background-color:pink;
  left:-300px;
  transition:all 1s;
}
#sidebar.active{
  left:0;
}
#sidebar .toggle{
  posotion:absolute;
  margin-left:310px;
}
.toggle span{
  width:30px;
  height:4px;
  background-color:black;
  display:block;
  margin-top:4px;
  
}


.logo{
  
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30%;

  a{
    text-decoration: none;
    color:black;
    padding:40px;
    
  }
  
  
}
h2{
    text-align: center;
    
}
form{
  input{
    margin-bottom: 15px;
    width:260px;
    height:20px;
    margin-left: 40%; 
  }
  button{
    background-color: #f1c40f;
    border:.5px solid #f1c40f;
    height:30px;
    width:140px;
    font-weight: bold;
    font-size: 17px;
    margin-left: 45%;
    margin-bottom: 80px; 
}
  }
}
.first{
    margin-left: 30%; 

    .spa{
        font-weight: bold;
        font-size: 23px;
    }
    p{
      margin-bottom: 60px; 
    }   
  }
  iframe{
    margin-left: 30%; 
    width:550px;
    margin-top: 10px; 
    margin-bottom: 60px; 
  }
  .one{
    display:flex;
    justify-content: space-around;
    max-width:77%;
      margin:auto;
  .second{
    width:320px;
    height:300px;
    border:1px solid #000;
    

    header{
      background-color:#ddd;
      color:black;
      width:100%;
      height:50px;
      font-size: 17px;
      text-align:center;
    }
    ol{
      list-style-type: none;
      margin-left:18%;
    }
    h3{
      margin-left:30%;
      font-size: 27px;
    }
    button{
      background-color: #f1c40f;
    border:.5px solid #f1c40f;
    height:40px;
    width:100px;
    margin-left:27%;
    }
  }
}



`


class App extends Component {

    render() {
      
        return (
          
          <div className={cx(R)} style={{background: '#eee'}}>
               <div id="sidebar">
              <div className="toggle" onClick={this.open}>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <h1>Hello</h1>
              <input type="text" name="main" placeholder="Enter your name"/>
            </div>

            <div id="sidebar">
              <div className="toggle">
                <span></span>
                <span></span>
                <span></span>
              </div>
              <input type="text" name="main" />
            </div>
            <div className="logo">
               <div className="image">
            <img src="https://s3.amazonaws.com/freecodecamp/original_trombones.png" width="300px"/>
            </div>
            <div className="fhp">
              <a href="#f">Features</a>
              <a href="#h">How It Works </a>
              <a href="#p">Pricing </a>
              </div>
              </div>
              <h2>Handcrafted, home-made masterpieces</h2>
              <form>
              <input name="email" type="text" placeholder="Enter your email address" /><br/>
              <button type="submit"> GET STARTED </button>
              </form>
            <div>
                <div className="first">
                   <p id="f"><span className="spa">Premium Materials</span><br/>Our trombones use the shiniest brass which is sourced locally. This will increase the longevity of your purchase.</p>
                </div>
                <div className="first">
                   <p><span className="spa">Fast Shipping</span><br/>We make sure you recieve your trombone as soon as we have finished making it. We also provide free returns if<br/> you are not satisfied.</p>
                </div>
                <div className="first">
                   <p><span className="spa">Quality Assurance</span><br/>For every purchase you make, we will ensure there are no damages or faults and we will check and test the pitch of<br/> your instrument.</p>
                </div>
                <iframe id="h" name="video" height="315" src="https://www.youtube-nocookie.com/embed/y8Yv4pnO7qc?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
            </div>
            <div className="one" id="p">
              <div className="second">
                <header>TENOR TROMBONE</header>
                <h3>$600</h3>
                <ol>
                <li>Lorem ipsum.</li>
                <li> Lorem ipsum.</li>
                <li> Lorem ipsum dolor</li>
                <li>Lorem ipsum.</li>
                </ol>
                 <button type="submit">SELECT</button>
                
              </div>
              <div className="second">
                <header>BASS TROMBONE</header>
                <h3>$900</h3>
                <ol>
                <li>Lorem ipsum.</li>
                <li> Lorem ipsum.</li>
                <li> Lorem ipsum dolor</li>
                <li>Lorem ipsum.</li>
                </ol>
                 <button type="submit">SELECT</button>
                
              </div>
              <div className="second">
                <header>VALVE TROMBONE</header>
                <h3>$1200</h3>
                <ol>
                <li>Lorem ipsum.</li>
                <li> Lorem ipsum.</li>
                <li> Lorem ipsum dolor</li>
                <li>Lorem ipsum.</li>
                </ol>
                 <button type="submit">SELECT</button>
              
              </div>

            </div>

            
          </div>
           
        )
    }


}

export default App;